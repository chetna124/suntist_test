import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { HomeService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userForm:FormGroup;
  results:any[]=[];
  personList:{name:string,age:number,sex:string}[] = [];
  constructor(private fb:FormBuilder,private homeService: HomeService) { }

  ngOnInit() {
    const defaultUser = {name:'Chetna Luthra (Default)', age:24, sex:'Female'};
    this.personList.push(defaultUser);
    this.userForm=this.fb.group({
      name:[],
      age:[],
      sex:[]
    });
    this.homeService.getUsers().subscribe((resp: any) => {
      this.results = resp.results;
    });
  }

  onFormSubmit(){
    this.personList.push(this.userForm.value);
  }

  setSex(sex: any){
    console.log(sex);
    this.userForm.value.sex = sex.value;
  }

}
