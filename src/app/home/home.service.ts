import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HomeService {
  url='https://api.suntist.com/aboutus/';
  constructor(private http:HttpClient) { }

  getUsers(){
    return this.http.get(this.url);
  }
}
